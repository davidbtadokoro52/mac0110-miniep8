# MAC0110-MiniEP8

This project is a Programming Exercise that focus on creating
algorithms, based on the Julia programming language, for sorting cards
of a deck by its value and kind. Also, getting more used to the Git
mechanics is an objective.
