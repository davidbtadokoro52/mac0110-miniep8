function buscaLinear(x, v)
	for i in 1:length(v)
		if v[i] == x
			return i
		end
	end
	return 0
end

function compareByValue(x, y)
	chart_of_order = ["2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"]
	val_x = buscaLinear(x[1:end-1], chart_of_order)
	val_y = buscaLinear(y[1:end-1], chart_of_order)
	return val_x < val_y
end

function compareByValueAndSuit(x, y)
	chart_of_kind = ["♦", "♠", "♥", "♣"]
	kind_x = buscaLinear(x[end:end], chart_of_kind)
	kind_y = buscaLinear(y[end:end], chart_of_kind)
	if kind_x != kind_y
		return kind_x < kind_y
	else
		return compareByValue(x, y)
	end
end

function troca(v, i, j)
	aux = v[i]
	v[i] = v[j]
	v[j] = aux
end
	
function insercao(v)
	tam = length(v)
	for i in 2:tam
		j = i
		while j > 1
			if compareByValueAndSuit(v[j], v[j - 1])
				troca(v, j, j - 1)
			else
				break
			end
			j = j - 1
		end
	end
	return v
end
