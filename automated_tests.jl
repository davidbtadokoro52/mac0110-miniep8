include("miniep8.jl")
using Test

function test_compareByValue()
	@test compareByValue("2♠", "A♠") == true
	@test compareByValue("K♥", "10♥") == false
	@test compareByValue("10♠", "10♥") == false
	@test compareByValue("7♣", "Q♦") == true
	@test compareByValueAndSuit("10♠", "10♥") == true
	@test compareByValueAndSuit("A♠", "2♥") == true
	@test compareByValueAndSuit("9♦", "9♦") == false
	@test compareByValueAndSuit("J♣", "K♦") == false
	@test insercao(["10♥", "10♦", "K♠", "A♠", "J♠", "A♠"]) == ["10♦", "J♠", "K♠", "A♠", "A♠", "10♥"]
	println("Tempo para comparação por valor apenas = ", @elapsed compareByValue("8♥", "K♥"))
	println("Tempo para comparação por valor e naipe = ", @elapsed compareByValueAndSuit("A♠", "2♥"))
	println("Tempo para ordenação por inserção = ", @elapsed insercao(["10♥", "10♦", "K♠", "A♠", "J♠", "A♠"]))
	println("Fim dos testes.")
end

test_compareByValue()
